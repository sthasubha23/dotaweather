angular.module('myApp').component('weatherCom', {
    templateUrl: 'components/weather/weather.html',
    controller: ['$scope', 'weatherServices', function ($scope, weatherServices) {
        try {

            var apikey = 'cc965723e32c8c8fdb5638ef5d5a2896';

            //UseEffect
            var defaultLocation = 'Kathmandu';
            $scope.initSearch = function () {
                $scope.searchFunc(defaultLocation);
            };

            //searchWeatherDetails
            $scope.searchFunc = function (cityName) {
                weatherServices.getWeather(cityName, apikey)
                    .then(function (response) {
                        var weatherData = response.data;
                        $scope.weatherCondition = weatherData.weather[0].main;
                        $scope.degree = (weatherData.main.temp - 273.15).toFixed(2);
                        $scope.wind = weatherData.wind.speed;
                        $scope.windGust = weatherData.wind.gust;
                        $scope.sunrise = new Date(weatherData.sys.sunrise * 1000).toLocaleTimeString();
                        $scope.sunset = new Date(weatherData.sys.sunset * 1000).toLocaleTimeString();
                    })
                    .catch(function (error) {
                        console.error("no data found", error);
                    });
            };
        } catch (error) {
            console.error("no data found", error);
        }
    }]
});