angular.module('myApp').service('weatherServices', function($http) {
    this.getWeather = function(cityName, apikey) {
        var url = 'https://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&appid=' + apikey;
        return $http.get(url);
    };
});