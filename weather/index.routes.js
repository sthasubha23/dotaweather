
angular.module('myApp', ['ngRoute']);
angular.module('myApp').config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'pages/home.html'
        })
        .when('/hero', {
            templateUrl: 'pages/hero.html'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
