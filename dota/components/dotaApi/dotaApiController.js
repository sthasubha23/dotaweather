angular.module('myApp').component('dotaApi', {
    templateUrl: 'components/dotaApi/dotaApi.html',
    controller: ['$http', '$scope','$rootScope','$location', function ($http, $scope,$rootScope,$location) {

        $http.get('https://api.opendota.com/api/heroStats').then(function (response) {
            if (response.data ) {
                console.log(response.data)
                $scope.heroes = response.data;
            } else {
                console.error('Invalid data format. Expected "heroes" array not found.');
            }
        }).catch(function (error) {
            console.error('Error fetching data:', error);
        });
        
        // $scope.logButtonClick = function (hero) {
        //     console.log('Button clicked',hero);
        //     $rootScope.selectedHero = hero
        //     $location.path('/hero/' + hero.name);
        // };
    }
    ]

});