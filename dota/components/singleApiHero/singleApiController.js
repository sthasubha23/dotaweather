angular.module('myApp').
    component('heroApi', {
        templateUrl: 'components/singleApiHero/singleApiHero.html',
        controller: ('singleApiController', ['$rootScope', '$scope', function ($rootScope, $scope) {
            $scope.selectedHero = $rootScope.selectedHero;
        }])
    })

