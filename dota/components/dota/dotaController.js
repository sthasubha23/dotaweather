angular.module('myApp').component('dotaCom', {
    templateUrl: 'components/dota/dota.html',
    controller: ['$http', '$scope','$rootScope','$location', function ($http, $scope,$rootScope,$location) {

        $http.get('components/dota/dota.json').then(function (response) {
            if (response.data && response.data.heroes) {
                $scope.heroes = response.data.heroes;
            } else {
                console.error('Invalid data format. Expected "heroes" array not found.');
            }
        }).catch(function (error) {
            console.error('Error fetching data:', error);
        });
        
        $scope.logButtonClick = function (hero) {
            console.log('Button clicked',hero);
            $rootScope.selectedHero = hero
            $location.path('/hero/' + hero.name);
        };
    }
    ]

});